extends Camera2D

func small_shake() -> void:
	$ScreenShake.start(.5, 15, 16, 0)
