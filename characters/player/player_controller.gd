extends KinematicBody2D

const MAX_HEALTH = 100.0

var speed = 600
var move_dir = Vector2(0, 0)
var aim_dir = Vector2(1, 0)
var health = MAX_HEALTH

var can_shoot = true
onready var shoot_timer = $ShootTimer

export(PackedScene) var bullet_scn
export(GradientTexture) var health_colour

func _shoot_timer_on_timeout():
	can_shoot = true

var timer = 0.5
func shoot():
	var instance = bullet_scn.instance()
	instance.position = $Node/Aim/bullet_spawn_01.get_global_transform().get_origin()
	instance.target = aim_dir.normalized()
	$Node.add_child(instance)
	instance = bullet_scn.instance()
	instance.position = $Node/Aim/bullet_spawn_02.get_global_transform().get_origin()
	instance.target = aim_dir.normalized()
	$Node.add_child(instance)

func _ready():
	shoot_timer.connect("timeout", self, "_shoot_timer_on_timeout")

func hit(damage = 0):
	health -= damage
	$Camera.small_shake()
	$AudioStreamPlayer2D.play()

func _process(delta):
	if not Input.is_action_pressed("main_command") and can_shoot:
		can_shoot = false
		shoot_timer.start(.1)
		shoot()
	if Input.is_mouse_button_pressed(BUTTON_LEFT):
		aim_dir = Vector2($Camera.get_global_mouse_position() - position).normalized()

	$Triangle.set_default_color(health_colour.get_gradient().interpolate(abs((health / MAX_HEALTH) - 1)))

	if health <= 0:
		get_parent().show_game_over_screen()

func _physics_process(_delta):
	move_dir.x = -Input.get_action_strength("move_left") + Input.get_action_strength("move_right")
	move_dir.y = +Input.get_action_strength("move_down") - Input.get_action_strength("move_up")

	var new_aim_dir = Vector2()
	new_aim_dir.x = -Input.get_action_strength("aim_left") + Input.get_action_strength("aim_right")
	new_aim_dir.y = +Input.get_action_strength("aim_down") - Input.get_action_strength("aim_up")

	if new_aim_dir != Vector2.ZERO:
		aim_dir = new_aim_dir

	if move_dir.length() > 1:
		move_dir = move_dir.normalized()
	var velocity = move_dir * speed
	velocity = move_and_slide(velocity)

	if position.x < -16:
		position.x += 4096 + 16
	if position.x > 4096 + 16:
		position.x -= 4096 + 16
	if position.y < -16:
		position.y += 4096 + 16
	if position.y > 4096 + 16:
		position.y -= 4096 + 16

	look_at(position + move_dir)
	$Node/Aim.position = position
	$Node/Aim.look_at($Node/Aim.position + aim_dir)
