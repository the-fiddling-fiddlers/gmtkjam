extends Area2D

const SPEED = 300

var damage = 20

func hit(damage):
	pass

func move_to(target_dir: Vector2, delta):
	position += target_dir * delta * SPEED
	if position.x < -16:
		position.x += 4096 + 16
	if position.x > 4096 + 16:
		position.x -= 4096 + 16
	if position.y < -16:
		position.y += 4096 + 16
	if position.y > 4096 + 16:
		position.y -= 4096 + 16
