extends "res://characters/enemies/enemy_square_passive.gd"

export(PackedScene) var bullet_scn

func _on_BulletTimer_timeout():
	var instance = bullet_scn.instance()
	instance.position = $BulletSpawn.get_global_transform().get_origin()
	instance.target = Vector2(player.get_global_transform().get_origin() - position).normalized()
	get_parent().add_child(instance)
	Score.total_score += 20
	$BulletTimer.start(2)

func _on_TriangleAggresive_body_entered(body):
	if body.is_in_group("player"):
		body.hit(damage)
		hit(damage)
