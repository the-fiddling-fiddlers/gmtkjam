extends "res://characters/enemies/base_enemy.gd"

var target = Vector2(0,0)
var target_rotation = 0

var player

func _ready():
	target = Vector2(rand_range(-1, 1), rand_range(-1, 1))
	target_rotation = rand_range(0, TAU)

func _physics_process(delta):
	move_to(target, delta)
	rotation += target_rotation * delta

var death = false
func hit(damage):
	if not death:
		$Particles2D.set_emitting(true)
		$Square.hide()
		$CollisionPolygon2D.call_deferred("set_disabled", true)
		Score.total_score += 10
		$Timer.start(4)

func _on_Timer_timeout():
	queue_free()

func _on_SquarePassive_body_entered(body):
	if body.is_in_group("player"):
		body.hit(damage)
		hit(damage)
