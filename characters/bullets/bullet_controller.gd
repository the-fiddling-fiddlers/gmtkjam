extends Node2D

var target = Vector2(0, 0)

export var speed = 1200

export(PackedScene) var explosion

export(String) var hittable_group
export(int) var damage

func _ready():
	look_at(position + target)

func hit(damage):
	if is_in_group("enemies"):
		Score.total_score += 5
	queue_free()

func _physics_process(delta):
	# TODO: raycast to see if we hit something inbetween positions
	position += target * delta * speed
	look_at(position + target)
	
	if position.x < -16:
		position.x += 4096 + 16
	if position.x > 4096 + 16:
		position.x -= 4096 + 16
	if position.y < -16:
		position.y += 4096 + 16
	if position.y > 4096 + 16:
		position.y -= 4096 + 16

func _on_Timer_timeout():
	queue_free()

func _on_Area2D_body_entered(body):
	if body.is_in_group(hittable_group):
		body.hit(damage)
		var instance = explosion.instance()
		instance.position = position
		get_parent().add_child(instance)
		queue_free()

func _on_Area2D_area_entered(area):
	if area.is_in_group(hittable_group):
		area.hit(damage)
		var instance = explosion.instance()
		instance.position = position
		get_parent().add_child(instance)
		queue_free()

func _on_Bullet_body_entered(body):
	if body.is_in_group(hittable_group):
		body.hit(damage)
		var instance = explosion.instance()
		instance.position = position
		get_parent().add_child(instance)
		queue_free()
