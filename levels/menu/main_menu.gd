extends Control

func _ready():
	get_tree().paused = false
	$Background/Background.pressed = Config.background

func _on_Start_pressed():
	var _result = get_tree().change_scene("res://levels/main/main_scene.tscn")

func _on_Exit_pressed():
	get_tree().quit()

func _on_Start_mouse_entered():
	$AudioButtons.play();

func _on_Exit_mouse_entered():
	$AudioButtons.play();

func _on_Bloom_pressed():
	Config.change_bloom()
	if Config.bloom == 0:
		$WorldEnvironment.environment.set_glow_level(1, false)
		$WorldEnvironment.environment.set_glow_level(2, false)
		$WorldEnvironment.environment.set_glow_level(3, false)
		$WorldEnvironment.environment.set_glow_level(4, false)
		$WorldEnvironment.environment.set_glow_level(5, false)
		$WorldEnvironment.environment.set_glow_level(6, false)
		$WorldEnvironment.environment.set_glow_level(7, false)
	if Config.bloom == 1:
		$WorldEnvironment.environment.set_glow_level(1, false)
		$WorldEnvironment.environment.set_glow_level(2, false)
		$WorldEnvironment.environment.set_glow_level(3, false)
		$WorldEnvironment.environment.set_glow_level(4, false)
		$WorldEnvironment.environment.set_glow_level(5, false)
		$WorldEnvironment.environment.set_glow_level(6, true)
		$WorldEnvironment.environment.set_glow_level(7, true)
	if Config.bloom == 2:
		$WorldEnvironment.environment.set_glow_level(1, false)
		$WorldEnvironment.environment.set_glow_level(2, false)
		$WorldEnvironment.environment.set_glow_level(3, false)
		$WorldEnvironment.environment.set_glow_level(4, false)
		$WorldEnvironment.environment.set_glow_level(5, true)
		$WorldEnvironment.environment.set_glow_level(6, true)
		$WorldEnvironment.environment.set_glow_level(7, true)
	if Config.bloom == 3:
		$WorldEnvironment.environment.set_glow_level(1, false)
		$WorldEnvironment.environment.set_glow_level(2, false)
		$WorldEnvironment.environment.set_glow_level(3, true)
		$WorldEnvironment.environment.set_glow_level(4, false)
		$WorldEnvironment.environment.set_glow_level(5, true)
		$WorldEnvironment.environment.set_glow_level(6, true)
		$WorldEnvironment.environment.set_glow_level(7, true)

func _on_Background_toggled(button_pressed):
	Config.background = button_pressed
