extends Node2D

export(PackedScene) var game_world_scn
var game_world

func _ready():
	game_world = game_world_scn.instance()
	game_world.music = "../Music"
	game_world.pause_mode = PAUSE_MODE_STOP
	add_child(game_world)
	game_world.connect("on_game_over", self, "_on_GameWorld_on_game_over")
	get_tree().paused = false

func _on_GameWorld_on_game_over():
	get_tree().paused = true
	$GameOverScreen/Menu.show()
	$GameOverScreen/Menu/Panel/Score.text = "Score: " + str(Score.total_score)

func _process(_delta):
	$GameOverScreen/Control/Panel/Label.text = "FPS: " + str(Engine.get_frames_per_second())
	$GameOverScreen/Control/Score/Label.text = "Score: " + str(Score.total_score)
	if Input.is_action_just_pressed("reset") and get_tree().paused:
		var _result = get_tree().change_scene("res://levels/main/main_scene.tscn")
	if Input.is_action_just_pressed("escape"):
		var _result = get_tree().change_scene("res://levels/menu/main_menu.tscn")
