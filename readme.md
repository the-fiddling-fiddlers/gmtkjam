# dreadshot
# !! SEIZURE WARNING !!

## ! THIS GAME CONTAINS FLASHING LIGHTS !

###Summary:

Mesmerizing twin-stick-shooter focused on an audio-visual experience.

Controls:

    WASD or left joystick to move
    hold left mouse button to aim with mouse, otherwise right joystick or IJKL
    Escape to go to menu
    R to restart if you game over

The colour of your triangle indicates your health, green = high, yellow = middle, red = low.

Made for the gmtkjam with the theme "out of control".

the out of control part is the graphics, as this is one of the first times we have made a game that focuses mainly on graphics.

https://gitlab.com/the-fiddling-fiddlers/gmtkjam/

Code is licensed under GPLv3

The Mac build has not been tested on the actual operating system,
If it doesn't work, please let me know!

Windows build runs fine under Gentoo using wine v4.0.2.
It has also been able to run on a Windows 10 system.

# ITCH Page

https://ragarock.itch.io/dreadshot
