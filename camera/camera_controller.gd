extends Camera2D

export(NodePath) var follow_target

func _ready():
	pass

func _process(delta):
	if(get_node(follow_target)):
		offset = offset.linear_interpolate(get_node(follow_target).position, delta * 8)
