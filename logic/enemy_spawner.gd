extends Node2D

export(Array, PackedScene) var enemy_types
export(NodePath) var player

const SPAWN_TIME = 4

func _ready():
	$SpawnTimer.start(SPAWN_TIME)

var timer = 0
func _on_SpawnTimer_timeout():
	timer += 1
	
	var j = 3 + floor(timer / 10)
	for t in range(j):
		$SpawnTimer.start(SPAWN_TIME)
		var i = floor(rand_range(0, enemy_types.size()))
		var instance = enemy_types[i].instance()
		instance.position = Vector2(rand_range(0, 4096), rand_range(0, 4096))
		instance.player = get_node(player)
		add_child(instance)
