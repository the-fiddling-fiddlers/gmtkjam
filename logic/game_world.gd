extends Node

signal on_game_over

export(NodePath) var music

var score = 0

func _ready():
	$ColorRect.material.set_shader_param("time_since_game_start", OS.get_ticks_msec() / 1000.0)
	get_node(music).play()
	if !Config.background:
		$ColorRect.hide()
	if Config.bloom == 0:
		$WorldEnvironment.environment.set_glow_level(1, false)
		$WorldEnvironment.environment.set_glow_level(2, false)
		$WorldEnvironment.environment.set_glow_level(3, false)
		$WorldEnvironment.environment.set_glow_level(4, false)
		$WorldEnvironment.environment.set_glow_level(5, false)
		$WorldEnvironment.environment.set_glow_level(6, false)
		$WorldEnvironment.environment.set_glow_level(7, false)
	if Config.bloom == 1:
		$WorldEnvironment.environment.set_glow_level(1, false)
		$WorldEnvironment.environment.set_glow_level(2, false)
		$WorldEnvironment.environment.set_glow_level(3, false)
		$WorldEnvironment.environment.set_glow_level(4, false)
		$WorldEnvironment.environment.set_glow_level(5, false)
		$WorldEnvironment.environment.set_glow_level(6, true)
		$WorldEnvironment.environment.set_glow_level(7, true)
	if Config.bloom == 2:
		$WorldEnvironment.environment.set_glow_level(1, false)
		$WorldEnvironment.environment.set_glow_level(2, false)
		$WorldEnvironment.environment.set_glow_level(3, false)
		$WorldEnvironment.environment.set_glow_level(4, false)
		$WorldEnvironment.environment.set_glow_level(5, true)
		$WorldEnvironment.environment.set_glow_level(6, true)
		$WorldEnvironment.environment.set_glow_level(7, true)
	if Config.bloom == 3:
		$WorldEnvironment.environment.set_glow_level(1, false)
		$WorldEnvironment.environment.set_glow_level(2, false)
		$WorldEnvironment.environment.set_glow_level(3, true)
		$WorldEnvironment.environment.set_glow_level(4, false)
		$WorldEnvironment.environment.set_glow_level(5, true)
		$WorldEnvironment.environment.set_glow_level(6, true)
		$WorldEnvironment.environment.set_glow_level(7, true)

func show_game_over_screen():
	emit_signal("on_game_over")

func add_score(new_score):
	score += new_score
